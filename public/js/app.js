routerApp = angular.module('todoAngular', ['controllers', 'services', 'directives', 'values', 'ui.router']);

routerApp.run(function ($state, $rootScope, Login) {
    Login.getUserData().then(function (data) {
        $rootScope.userData = data;
    }, function (error) {
        $rootScope.userData = error;
    })


    /**
     * Log out method
     * */
    $rootScope.logOut = function () {
        localStorage.clear();
        $state.go('login');
        $rootScope.userData = null;
    };
});
routerApp.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'modules/login/loginTemplate.html',
            controller: "loginCtrl"
        })
        .state('todo', {
            url: '/todo',
            templateUrl: 'modules/todo/todoTemplate.html',
            controller: "todoCtrl"
        })
        .state('profile', {
            url: '/profile',
            templateUrl: 'modules/profile/profileTemplate.html',
            controller: "profileCtrl"
        })

    var userData = localStorage.getItem(Strings.UserObject);

    if (userData)
        $urlRouterProvider.otherwise('/todo');
    else
        $urlRouterProvider.otherwise('/login');
});