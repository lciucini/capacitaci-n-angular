values
    .value("tempStorage", {});
services
    .factory('Router', function ($state, tempStorage) {
        return {
            /**
             * Description
             * @method goTo
             * @param {} url
             * @param {} object
             * @return
             */
            goTo: function (url, object) {
                if (object != undefined)
                    tempStorage.args = object;

                $state.go(url);
            }
        }
    });
