controllers
    .controller('loginCtrl', ['$scope', '$rootScope', '$http', '$state', 'Login', 'Router', function ($scope, $rootScope, $http, $state, Login, Router) {
        /**
         * Initialised data user variable*/
        $scope.dataUser = {};

        /**
         * Login Method
         * */
        $scope.login = function () {
            Login.login($scope.dataUser).then(function (data) {
                Login.saveUserData(data).then(function (data) {
                    $rootScope.userData = data; // Alcance global
                    Router.goTo('todo', {user: 'test'});
                })
            }, function (error) {
                console.log(error);
            })
        };
    }]);