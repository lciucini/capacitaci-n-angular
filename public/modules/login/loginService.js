services
    .factory('Login', ['$http', '$q', function ($http, $q) {
        return {
            login: function (user) {
                // create promise
                var deferred = $q.defer();

                // setting http request (ajax)
                $http({
                    method: 'POST',
                    url: Strings.ApiLogin,
                    data: user,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).success(function (data) {
                    // return promise callback
                    deferred.resolve(data);
                }).error(function (error) {
                    // return error callback
                    deferred.reject(error);
                })

                // return promise
                return deferred.promise;
            },
            create: function (todoData) {
                // simply mode of return callback http request
                return $http.post(Strings.ApiEndPoint, todoData);
            },
            delete: function (id) {
                return $http.delete(Strings.ApiEndPoint + id);
            },
            /**
             * Local Storage Method to save JSON object
             * */
            saveUserData: function (userData) {
                var userDataSave;
                var deferred = $q.defer();

                userData = JSON.stringify(userData);
                localStorage.setItem(Strings.UserObject, userData);
                userDataSave = localStorage.getItem(Strings.UserObject);

                if (userDataSave) {
                    userDataSave = JSON.parse(userDataSave);
                    deferred.resolve(userDataSave);
                }
                else {
                    deferred.reject(null);
                }

                return deferred.promise;
            },
            getUserData: function () {
                var deferred = $q.defer();

                var userDataSave = localStorage.getItem(Strings.UserObject);

                if (userDataSave) {
                    userDataSave = JSON.parse(userDataSave);
                    deferred.resolve(userDataSave);
                }
                else {
                    deferred.reject(null);
                }

                return deferred.promise;
            }
        }
    }]);