services
// super simple service
// each function returns a promise object
    .factory('Todos', ['$http', function ($http) {
        return {
            get: function () {
                return $http.get(Strings.ApiEndPoint);
            },
            create: function (todoData) {
                return $http.post(Strings.ApiEndPoint, todoData);
            },
            delete: function (id) {
                return $http.delete(Strings.ApiEndPoint + id);
            }
        }
    }]);